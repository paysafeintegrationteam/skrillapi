﻿/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace SkrillAPI
{
	class SkrillAPITransaction
	{
		private string amount;
		private string currency;
		private string descriptor;
		private string trnID;
		private string confirmationNote;
		private string detail1Description;
		private string detail1Text;

		public void setAmount(string amount)
		{
			this.amount = amount;
		}

		public void setCurrency(string currency)
		{
			this.currency = currency;
		}

		public void setDescriptor(string descriptor)
		{
			this.descriptor = descriptor;
		}

		public void setTrnID(string trnID)
		{
			this.trnID = trnID;
		}

		public void setConfirmationNote(string confirmationNote)
		{
			this.confirmationNote = confirmationNote;
		}
		
		public void setDetail1Description(string detail1Description)
		{
			this.detail1Description= detail1Description;
		}
		
		public void setDetail1Text(string detail1Text)
		{
			this.detail1Text = detail1Text;
		}

		public string getAmount()
		{
			return this.amount;
		}

		public string getCurrency()
		{
			return this.currency;
		}

		public string getDescriptor()
		{
			return this.descriptor;
		}

		public string getTrnID()
		{
			return this.trnID;
		}

		public string getConfirmationNote()
		{
			return this.confirmationNote;
		}

		public string getDetail1Description()
		{
			return this.detail1Description;
		}
		
		public string getDetail1Text()
		{
			return this.detail1Text;
		}


	}

	class SkrillAPIDebit
	{
		private string notificationURL;
		private string successURL;
		private string cancelURL;
		private string logo_url;

		public void setNotificationURL(string notificationURL)
		{
			this.notificationURL = notificationURL;
		}

		public void setSuccessURL(string successURL)
		{
			this.successURL = successURL;
		}

		public void setCancelURL(string cancelURL)
		{
			this.cancelURL = cancelURL;
		}

		public void setLogoURL(string logo_url)
		{
			this.logo_url = logo_url;
		}

		public string getNotificationURL()
		{
			return this.notificationURL;
		}

		public string getSuccessURL()
		{
			return this.successURL;
		}

		public string getCancelURL()
		{
			return this.cancelURL;
		}

		public string getLogoURL()
		{
			return this.logo_url;
		}
	}

	class SkrillAPI
	{
		private HttpClient httpClient;
		private SkrillAPIConfig skrillConfig;
		private SkrillAPICustomer customer;
		private SkrillAPIDebit debit;
		private SkrillAPITransaction transaction;

		public SkrillAPI(HttpClient httpClient, SkrillAPIConfig config)
		{
			this.httpClient = httpClient;
			this.skrillConfig = config;
		}

		public void customerData(SkrillAPICustomer customer)
		{
			this.customer = customer;
		}

		public void debitData(SkrillAPIDebit debit)
		{
			this.debit = debit;
		}

		public void transactionData(SkrillAPITransaction transaction)
		{
			this.transaction = transaction;
		}

		public string debitTransactionData()
		{
			return this.httpClient.post(this.body());
		}

		public Dictionary<string, string> body()
		{

			Dictionary<string, string> writer = new Dictionary<string, string>();
		
			writer.Add("pay_to_email", this.skrillConfig.getMerchantEmail());
			writer.Add("currency", this.transaction.getCurrency());
			writer.Add("amount", this.transaction.getAmount());
			writer.Add("prepare_only", "1");
			writer.Add("recipient_description", this.transaction.getDescriptor());
			writer.Add("detail1_description", this.transaction.getDetail1Description());
			writer.Add("detail1_text", this.transaction.getDetail1Text());
			writer.Add("return_url", this.debit.getSuccessURL());
			writer.Add("cancel_url", this.debit.getCancelURL());
			writer.Add("status_url", this.debit.getNotificationURL());
			writer.Add("logo_url", this.debit.getLogoURL());
			writer.Add("language", this.customer.getLanguage());
			writer.Add("confirmation_note", this.transaction.getConfirmationNote());
			writer.Add("transaction_id", this.transaction.getTrnID());
			writer.Add("pay_from_email", this.customer.getEmail());
			writer.Add("firstname", this.customer.getFirstName());
			writer.Add("lastname", this.customer.getLastName());
			writer.Add("address", this.customer.getAddress1());
			writer.Add("address2", this.customer.getAddress2());
			writer.Add("phone_number", this.customer.getPhone());
			writer.Add("postal_code", this.customer.getZip());
			writer.Add("city", this.customer.getCity());
			writer.Add("state", this.customer.getState());
			writer.Add("country", this.customer.getCountry());
		
			return writer;
		}

	}

	class SkrillAPIConfig
	{
		private string merchantEmail;

		public SkrillAPIConfig(string merchantEmail)
		{
			this.merchantEmail = merchantEmail;
		}

		public string getMerchantEmail()
		{
			return this.merchantEmail;
		}

	}

	class SkrillAPICustomer
	{
		private string firstName;
		private string lastName;
		private string address1;
		private string address2;
		private string zip;
		private string city;
		private string state;
		private string country;
		private string language;
		private string phone;
		private string email;

		public void setFirstName(string firstName)
		{ 
			this.firstName = firstName;
		}

		public void setLastName(string lastName)
		{ 
			this.lastName = lastName;
		}

		public void setAddress1(string address1)
		{ 
			this.address1 = address1;
		}

		public void setAddress2(string address2)
		{ 
			this.address2 = address2;
		}

		public void setZip(string zip)
		{ 
			this.zip = zip;
		}

		public void setCity(string city)
		{ 
			this.city = city;
		}

		public void setState(string state)
		{ 
			this.state = state;
		}

		public void setCountry(string country)
		{ 
			this.country = country;
		}

		public void setLanguage(string language)
		{ 
			this.language = language;
		}

		public void setPhone(string phone)
		{ 
			this.phone = phone;
		}

		public void setEmail(string email)
		{ 
			this.email = email;
		}

		public string getFirstName()
		{ 
			return this.firstName;
		}

		public string getLastName()
		{ 
			return this.lastName;
		}

		public string getAddress1()
		{ 
			return this.address1;
		}

		public string getAddress2()
		{ 
			return this.address2;
		}

		public string getZip()
		{ 
			return this.zip;
		}

		public string getCity()
		{ 
			return this.city;
		}

		public string getState()
		{ 
			return this.state;
		}

		public string getCountry()
		{ 
			return this.country;
		}

		public string getLanguage()
		{ 
			return this.language;
		}

		public string getPhone()
		{ 
			return this.phone;
		}

		public string getEmail()
		{ 
			return this.email;
		}

	}

	class HttpClient
	{
		private string url;

		public HttpClient(string url)
		{
			this.url = url;
		}

		public string MakeHttpRequest(string method, string path, Dictionary<string, string> requestParams)
		{
			string url = path;
	  
			WebRequest request = WebRequest.Create(url);
	
			request.ContentType = "application/x-www-form-urlencoded";
	
			request.Method = method;
	   
			if (requestParams != null) {
				string queryBody = "";
				foreach (var item in requestParams) {
					queryBody +=  item.Key + "=" + Uri.EscapeDataString(item.Value) + "&";
				}
  	
				queryBody = queryBody.TrimEnd('&');
	    
				byte[] buf = Encoding.UTF8.GetBytes(queryBody);
				request.ContentLength = buf.Length;
				request.GetRequestStream().Write(buf, 0, buf.Length);
			}
	
			try {
				WebResponse response = request.GetResponse();
				Stream dataStream = response.GetResponseStream();
	
				if (dataStream == null) {
					throw new NullReferenceException();
				}
	
				StreamReader reader = new StreamReader(dataStream);
	
				string responseFromServer = reader.ReadToEnd();
	
				reader.Close();
				response.Close();
				return responseFromServer;
			} catch (WebException e) {
				Console.WriteLine("{0} Exception caught.", e);
			}
			return null;
		}

		public String post(Dictionary<string, string> body)
		{
		
			return MakeHttpRequest("POST", this.url,body);
		}
	}
}
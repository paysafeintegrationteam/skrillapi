﻿using System;

namespace SkrillAPI
{
	class Program
	{
		public static void Main(string[] args)
		{
			String url = "https://pay.skrill.com";
			SkrillAPIConfig config = new SkrillAPIConfig("SkrillPaymentHB3@skrill.com");
			HttpClient httpClient = new HttpClient(url);
			SkrillAPI skrillrequest = new SkrillAPI(httpClient, config);
			SkrillAPICustomer customer = new SkrillAPICustomer();
				customer.setFirstName("Firs%tname");
				customer.setLastName("Lastname");
				customer.setAddress1("address1");
				customer.setAddress2("address2");
				customer.setZip("123");
				customer.setCity("NY");
				customer.setState("NY");
				customer.setCountry("USA");
				customer.setLanguage("EN");
				customer.setPhone("123456");
				customer.setEmail("test+Customer999@skrill.com");
				
				skrillrequest.customerData(customer);
			SkrillAPIDebit debit = new SkrillAPIDebit();
				debit.setNotificationURL("https://your.notification.url.com/notification.html?param1=value1");
				debit.setSuccessURL("https://your.success.url.com/success.html?param2=value2");
				debit.setCancelURL("https://your.cancel.url.com/error.html?param3=value3");
				debit.setLogoURL("https://your.logo.url.com/logo.png");
			
				skrillrequest.debitData(debit);
			SkrillAPITransaction transaction = new SkrillAPITransaction();
				transaction.setAmount("2");
				transaction.setCurrency("EUR");
				transaction.setDescriptor("Tes%2 tMerchant");
				transaction.setTrnID("1");
				transaction.setConfirmationNote("Thank you for your payment");
				transaction.setDetail1Description("Product");
				transaction.setDetail1Text("43211234");
				
				skrillrequest.transactionData(transaction);
			String result = skrillrequest.debitTransactionData();
			
			Console.Write(result);
			Console.ReadKey(true);
		}
	}
}